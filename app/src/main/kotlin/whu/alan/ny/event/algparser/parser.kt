package parser

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader


/**@author Alan WANG
 * Created by Alan_WANG on 5/24/2017.
 */

/**
 * Read source Code
 * @param
 */
fun read(bis: InputStream): Script {
    val scp = Script()
    bis.use {
        //create buffer reader
        val a = BufferedReader(InputStreamReader(it)).readText()
        val events = a.split(Regex("""\n(?=.+?:\n)"""))
        for (e in events){
            scp.add(makeEvent(e))
        }
    }
    return scp
}

fun makeEvent(e: String): Event {
    var src = e.split(Regex(""":(?=\s+)"""))
    val result = Event(src[0].replace(Regex("\\s"),""))
    val exprs = src[1].split(Regex("""(?=\s[?#\[])"""))
    for (expr in exprs){
        if (!expr.matches(Regex("\\s*")))
        result.add(makeExpr(expr))
    }
    return result
}

fun makeExpr(expr: String): Expr {
    var dsafk = expr.matches(Regex("""\s*\[(.+):(.+)]\s*"""))
    if (expr.matches(Regex("""\s*#(.+)?#((\n|.)*$)"""))){
        val cs = Regex("""\s*#([a-zA-Z\u4e00-\u9fff]+)#((\n|.)*)$""").find(expr)
        return  Conversation(role = cs!!.groupValues[1],content = cs.groupValues[2])
    }else if (expr.matches(Regex("""\s*\[(.+):(.+)]\s*"""))){
        val cs = Regex("""\s*\[(.+):(.+)]\s*""").find(expr)
        return  Envset(key = cs!!.groupValues[1],value = cs.groupValues[2])
    }else{
        var ques = Regex("""\?(.*)\n""").find(expr)!!.groupValues[1]
        var ans_map = HashMap<String,String>()
        var items = Regex("""!(.*)>(.*)(\s|$)""")
        for (values in items.findAll(expr)){
            ans_map[values.groupValues[1]] = values.groupValues[2]
        }
        return Goto(ques,ans_map)
    }
}

