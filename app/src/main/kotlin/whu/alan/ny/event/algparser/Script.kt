package parser

/**@author Alan WANG
 * Created by Alan_WANG on 5/31/2017.
 */
class Script(){
    private var events = HashMap<String,Event>()
    private lateinit var last : Event
    fun add(x : Event){
        last = x
        events.put(x.name,x)
    }
}
class Event(var name:String){
    private var exprs = ArrayList<Expr>()
    fun add(x: Expr) {
        exprs.add(x)
    }
}
sealed class Expr
data class Conversation(val role : String,val content:String):Expr()
data class Goto(val ques:String,val ans:HashMap<String,String>):Expr()
data class Envset(val key:String,val value:String):Expr()