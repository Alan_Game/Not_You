package whu.alan.ny

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import org.jetbrains.anko.verticalLayout
import whu.alan.ny.UI.views.gameView

/**@author Alan WANG
 * Created by Alan_WANG on 5/16/2017.
 */
class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        verticalLayout {
            gameView()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}