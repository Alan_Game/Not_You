package whu.alan.ny

import android.app.Application
import android.content.Context
import android.media.MediaPlayer
import whu.alan.ny.Game.Companion.instance

/**Game is the singleton class , which stores all global variables the application should use
 * @property parser the alan game scripts parsers
 * @see [parser])
 * @property media the background music player
 * @see [MediaPlayer]
*@property instance companion object property , the singleton instance of [Game]
 * @author Alan WANG
 * @copyright ALan reserves all rights
 * Created by Alan_WANG on 5/21/2017.
 */
class Game :Application(){
    inner class Enviroment{

    }
    @Deprecated("ctx is not recommand usage is this version. you should call function get applicationContext\n" +
            "instead if you want to get Application context",level = DeprecationLevel.ERROR)
    val ctx:Context by lazy{this.applicationContext}
    companion object{
        lateinit var instance : Game
    }

    /**
     * Called when application starts
     */
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    /**
     * Create [MediaPlayer] object from resID
     * @param res resource ID
     * @see [MediaPlayer.create]
     */
    fun createPlayer(res: Int,play: Boolean = true,loop: Boolean = true): Unit {
        //media = MediaPlayer.create(applicationContext,R.raw.calvaria)
    }
}