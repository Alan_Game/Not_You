package whu.alan.ny.UI.views.callbacks

import android.view.SurfaceHolder
import android.view.SurfaceView

/**@author Alan WANG
 * Created by Alan_WANG on 5/16/2017.
 */

inline fun SurfaceView.callback(x : SurfaceHolder.()->Unit){
    holder.x()
}
inline fun SurfaceHolder.onChange(crossinline x:(SurfaceHolder, Int, Int, Int)->Unit){
    addCallback(object : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
            x(holder!!,format,width,height)
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {

        }

        override fun surfaceCreated(holder: SurfaceHolder?) {

        }
    })
}

inline fun SurfaceHolder.onCreate(crossinline x:(SurfaceHolder)->Unit){
    addCallback(object : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {

        }

        override fun surfaceCreated(holder: SurfaceHolder?) {
            x(holder!!)
        }
    })
}

inline fun SurfaceHolder.onDestroy(crossinline x:(SurfaceHolder)->Unit){
    addCallback(object : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
            x(holder!!)
        }

        override fun surfaceCreated(holder: SurfaceHolder?) {

        }
    })
}