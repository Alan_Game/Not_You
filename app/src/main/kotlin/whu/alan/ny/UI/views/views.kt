@file:Suppress("NOTHING_TO_INLINE")

package whu.alan.ny.UI.views

//import java.lang.Thread.yield
import android.content.Context
import android.graphics.*
import android.util.DisplayMetrics
import android.view.ViewManager
import org.jetbrains.anko.custom.ankoView

/**@author Alan WANG
 * Created by Alan_WANG on 5/16/2017.
 */
inline fun ViewManager.logoView(init: LogoView.() -> Unit): LogoView {
    return ankoView({ ctx: Context -> LogoView(ctx) }) { init() }
}

inline fun ViewManager.logoView(): LogoView = logoView({})

inline fun ViewManager.gameView(init: GameView.() -> Unit): GameView {
    return ankoView({ ctx: Context -> GameView(ctx) }) { init() }
}

inline fun ViewManager.gameView(): GameView = gameView({})

fun Canvas.drawBitmapC(src: Bitmap, center: Point, paint: Paint) {
    val rect0 = Rect(0, 0, src.width, src.height)
    val dx = center.x - src.width / 2
    val dy = center.y - src.height / 2
    val rect1 = Rect(rect0.left + dx, rect0.top + dy, rect0.right + dx, rect0.bottom + dy)
    this.drawBitmap(src, rect0, rect1, paint)
}

fun Canvas.drawBitmapF(src: Bitmap, x: Float, y: Float, metric: DisplayMetrics, paint: Paint) {
    val p = Point((metric.widthPixels * x).toInt(), (metric.heightPixels * y).toInt())
    this.drawBitmapC(src, p, paint)
}