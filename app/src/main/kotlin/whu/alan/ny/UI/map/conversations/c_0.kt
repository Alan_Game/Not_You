package whu.alan.ny.UI.map.conversations

/**@author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.media.MediaPlayer
import whu.alan.ny.R
import whu.alan.ny.UI.map.Map
import whu.alan.ny.UI.rio.imageio.Character

class c_0(parent: Map): Conversation(parent){
    var ouyang = parent.ctx.Character("ouyang_shanpao_0.png")
    var rects = Paint().apply {
        color = Color.argb(80,0,20,255)
    }
    var havesaid = false
    val paint = Paint()
    override fun start(cns: Canvas) {
        cns.drawRect(0f,parent.srn_height/2f,parent.srn_width.toFloat(),parent.srn_height.toFloat(),rects)
        cns.drawBitmap(ouyang,0f,0f, paint)
        if (!havesaid){
            MediaPlayer.create(parent.ctx, R.raw.ouyang0).start()
            havesaid = true
        }
    }

    val cvstion = mapOf<String,String>(
            Pair("ouyang_shanpac_0.png","安静!!等会入学考试开始了,把校园卡放桌子右上角.")
    )
}