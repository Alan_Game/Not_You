package whu.alan.ny.UI.map.conversations

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.text.TextPaint

/**@author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */
abstract class Conversation(val parent: whu.alan.ny.UI.map.Map){
    val font_en = Typeface.createFromAsset(parent.ctx.assets,"fonts/stocky.ttf")
    val font_cn = Typeface.createFromAsset(parent.ctx.assets, "fonts/font_cn.ttf")
    val m_tpaint = TextPaint().apply {
        color = Color.WHITE
        typeface = font_cn
        textSize *= 8
    }
    abstract fun start(cns: Canvas)
}

data class Dialogue(val id : Int,
                    val role : Role,
                    val text : String,
                    val dub : Int,
                    val next : (Int)->Int)

data class Role(val id : Int,
                val name : String,
                val figures : Array<Int>)
