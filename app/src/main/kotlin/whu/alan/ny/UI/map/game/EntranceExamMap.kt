package whu.alan.ny.UI.map.game

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import whu.alan.ny.Game
import whu.alan.ny.R
import whu.alan.ny.UI.map.Map
import whu.alan.ny.UI.map.conversations.c_0
import whu.alan.ny.UI.rio.imageio.BgImg

/**@author Alan WANG
 * Created by Alan_WANG on 5/19/2017.
 */
class EntranceExamMap(ctx: Context = Game.instance.applicationContext) : Map(ctx){
    override val m_paint: Paint = Paint()
    override val bg_music: Int = R.raw.moon_on_water
    val ds= c_0(this)
    override fun draw(cns: Canvas) {
        cns.drawBitmap(ctx.BgImg("classroom_0.jpg"),0f,0f,m_paint)
        ds.start(cns)
    }

}