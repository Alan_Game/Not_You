package whu.alan.ny.UI.rio.imageio

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import org.xmlpull.v1.XmlPullParser.*
import whu.alan.ny.R

/**@author Alan WANG
 * Created by Alan_WANG on 5/16/2017.
 */

fun Context.grabText(id: String): Bitmap {
    var t: Int = 0
    var l: Int = 0
    var r: Int = 0
    var b: Int = 0
    val xrp = resources.getXml(R.xml.texts)
    loop@ while (xrp.eventType != END_DOCUMENT) {
        when (xrp.eventType) {
            START_TAG -> {
                if (xrp.idAttribute == id) {
                    Log.d("XML", xrp.name)
                    while (xrp.next() == START_TAG) {
                        when (xrp.name) {
                            "top" -> {
                                t = xrp.nextText().toInt()
                            }
                            "left" -> {
                                l = xrp.nextText().toInt()
                            }
                            "right" -> {
                                r = xrp.nextText().toInt()
                            }
                            "bottom" -> {
                                b = xrp.nextText().toInt()
                            }
                        }
                    }
                    break@loop
                }
            }
        }
        Log.d("XML","${when(xrp.next()){
            START_TAG->{"start_tag"}
            END_TAG->{"end_tag"}
            TEXT->{"text"}
            else->{xrp.eventType}
        }}")
    }
    Log.d("XML", "$t $r $b $l")
    val decodeStream = BitmapFactory.decodeStream(resources.assets.open("texts/android_res.png"))
    return Bitmap.createBitmap(decodeStream,l,t,r-l,b-t)
}

fun Context.BgImg(name: String): Bitmap {
    return BitmapFactory.decodeStream(resources.assets.open("bgs/$name"))
}

fun Context.Character(name: String): Bitmap {
    return BitmapFactory.decodeStream(resources.assets.open("character/$name"))
}