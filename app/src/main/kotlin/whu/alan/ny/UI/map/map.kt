package whu.alan.ny.UI.map

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.MotionEvent
import org.jetbrains.anko.displayMetrics
import whu.alan.ny.Game
import whu.alan.ny.UI.views.GameView
import whu.alan.ny.UI.views.widget.Widget

/**@author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */

abstract class Map(val ctx: Context = Game.instance.applicationContext) {
    val srn_width = ctx.displayMetrics.widthPixels
    val srn_height = ctx.displayMetrics.heightPixels
    val widgets = ArrayList<Widget>()
    open val bg_music : Int = 0
    abstract val m_paint : Paint

    abstract fun draw(cns : Canvas)
    open fun onActionDown(view: GameView, event: MotionEvent): Boolean = true
    open fun onActionMove(view: GameView, event: MotionEvent): Boolean = true
    open fun onActionUp(view: GameView, event: MotionEvent): Boolean = true

}

fun Map.drawBgImg(name: String): Unit {

}