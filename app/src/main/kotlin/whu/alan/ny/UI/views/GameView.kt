package whu.alan.ny.UI.views

import android.content.Context
import android.graphics.Canvas
import android.view.SurfaceView
import whu.alan.ny.UI.map.Map
import whu.alan.ny.UI.views.callbacks.callback
import whu.alan.ny.UI.views.callbacks.onCreate
import kotlin.concurrent.thread

/**@author Alan WANG
 * Created by Alan_WANG on 5/16/2017.
 */
class GameView(ctx: Context) : SurfaceView(ctx) {
//    val map_stack = object : Stack<Map>(){
//        override fun push(item: Map): Map {
//            super.push(item)
//            crt_map = peek()
//            if (crt_map.bg_music != 0) MainActivity.Companion.Companion.changeBgMusic(crt_map.bg_music)
//            return peek()
//        }
//    }

    lateinit  var crt_map : Map
    var cns : Canvas? = null

    init {
        callback {
            onCreate {
                thread {
                    while (this@GameView.isEnabled) {
                    }
                }
            }
        }
    }

    fun drawMap (condition: ()->Boolean){
        if (condition()) crt_map.draw(cns!!)
    }
}