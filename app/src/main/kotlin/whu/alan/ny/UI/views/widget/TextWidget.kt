//package whu.alan.ny.widget
//
//import android.graphics.Canvas
//import android.graphics.Rect
//import whu.alan.ny.map.conversations.ConversationMap
//
///**@author Alan WANG
// * Created by Alan_WANG on 5/18/2017.
// */
//class TextWidget(parent: ConversationMap, stringId : Int) : Widget(parent) {
//    @Suppress("JoinDeclarationAndAssignment")
//    private var stringValue : String
//    init {
//        stringValue = parent.ctx.getString(stringId)
//        @Suppress("CanBeVal")
//        var _bounds : Rect = Rect()
//        parent.m_tpaint.getTextBounds(stringValue,0,stringValue.length,_bounds)
//        width = _bounds.width().toFloat()
//        height = _bounds.height().toFloat()
//    }
//
//    fun centerX(x: Float): Unit {
//        this.x = x * parent.srn_width - width/2
//    }
//    fun centerY(y: Float): Unit {
//        this.y = y * parent.srn_height - height/2f
//    }
//
//    fun centerAt(x: Float, y: Float): Unit {
//        centerX(x)
//        centerY(y)
//    }
//
//    override fun draw(cns: Canvas) {
//        super.draw(cns)
//        cns.drawText(stringValue,x,y+height,(parent as ConversationMap).m_tpaint)
//    }
//}