package whu.alan.ny.UI.views.widget
import whu.alan.ny.UI.map.Map
import whu.alan.ny.util.isInside

/** **Widget** is a abstract class to place on the [Map].
 * Similarly as [Map] is a light weight Activity in the whu.alan.ny.Game, [Widget] is a light weight
 * view.
 * @author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */

abstract class Widget(val parent : Map){
    var x : Float = 0f
    var y : Float = 0f
    var width : Float = 0f
    var height : Float = 0f
    var visible = true
    open fun draw(cns: android.graphics.Canvas){
        if (!visible) return
    }
    fun contains(x: Float, y: Float): Boolean {
        return (x.isInside(this.x,this.x+width) && y.isInside(this.y,this.y + height))
    }
}