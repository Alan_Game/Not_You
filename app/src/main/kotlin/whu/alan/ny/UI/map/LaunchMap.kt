package whu.alan.ny.UI.map

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import org.jetbrains.anko.displayMetrics
import whu.alan.ny.Game
import whu.alan.ny.R
import whu.alan.ny.UI.rio.imageio.BgImg
import whu.alan.ny.UI.rio.imageio.grabText
import whu.alan.ny.UI.views.drawBitmapF
import whu.alan.ny.util.breath

/**Launch Map , first map when game starts.
 * @author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */
class LaunchMap(ctx: Context = Game.instance.applicationContext) : Map(ctx) {
    // alpha value of str "Press to Start"
    private val v = breath(0, 0, 5, 255)
    // start button
    private val start_info = ctx.grabText("start_info")
    // whu.alan.ny.Game Title "Not You"
    private val title = ctx.grabText("title")
    //Launch map's background Image
    private val bg = ctx.BgImg("bg_launch.png")
    // mask alpha , decreased by time
    private var mask_alpha = 255
    override val bg_music: Int = R.raw.calvaria
    override val m_paint = Paint().apply {
        isAntiAlias = true
    }

    override fun draw(cns : Canvas) {
        cns.drawColor(Color.BLACK)
        // Background Image fade in
        m_paint.alpha = 255 - mask_alpha
        cns.drawBitmap(bg, 0f, 0f, m_paint)
        cns.drawBitmapF(title, 0.5f, 0.4f, ctx.displayMetrics, m_paint)
        mask_alpha = if (mask_alpha < 5) 0 else mask_alpha - 5

        //draw start button
        if(mask_alpha == 0) {
            m_paint.alpha = v.next()
            cns.drawBitmapF(start_info, 0.5f, 0.8f, ctx.displayMetrics, m_paint)
        }
    }

//    override fun onActionDown(view: GameView, event: MotionEvent): Boolean {
//
//        //check if start button chosen
//        if(event.y.isInside(srn_height * 0.7f,srn_height * 0.9f) && mask_alpha == 0){
//            view.map_stack.push(whu.alan.ny.map.game.EntranceExamMap())
//        }
//        return super.onActionDown(view, event)
//    }
}