package whu.alan.ny

import android.app.Activity
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import org.jetbrains.anko.displayMetrics
import org.jetbrains.anko.surfaceView
import org.jetbrains.anko.verticalLayout
import whu.alan.ny.UI.rio.imageio.grabText
import whu.alan.ny.UI.views.callbacks.callback
import whu.alan.ny.UI.views.callbacks.onCreate
import whu.alan.ny.UI.views.drawBitmapF
import whu.alan.ny.util.clock.schedule
import kotlin.concurrent.thread
import kotlin.coroutines.experimental.buildIterator

class WelcomeActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        verticalLayout {
            surfaceView {
                callback {
                    onCreate {
                        val genPaints= buildIterator<Paint> {
                            var alpha = 0
                            val pit = Paint()
                            while (true){
                                alpha = if (alpha + 1 <= 255) (alpha + 1) else 255
                                pit.alpha = alpha
                                yield(pit)
                            }
                        }
                        thread {
                            schedule(1000 / 30) {
                                var cns: Canvas? = null
                                synchronized(holder!!) {
                                    cns = holder.lockCanvas()
                                    cns!!.drawColor(Color.BLACK)
                                    cns!!.drawBitmapF(grabText("logo"),0.5f,0.5f,displayMetrics,genPaints.next())
                                }
                                holder.unlockCanvasAndPost(cns!!)
                            }
                        }
                    }
                }
            }
        }
    }
}
