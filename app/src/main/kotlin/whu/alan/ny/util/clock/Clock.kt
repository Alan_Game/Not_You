package whu.alan.ny.util.clock

import android.os.SystemClock

/**@author Alan WANG
 * Created by Alan_WANG on 5/15/2017.
 */
inline fun schedule(delay:Long, task: () -> Unit){
    val start = SystemClock.currentThreadTimeMillis()
    while (SystemClock.currentThreadTimeMillis() - start < delay){}
    task.invoke()
}