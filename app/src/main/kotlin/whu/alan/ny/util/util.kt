package whu.alan.ny.util

import android.util.Log
import java.lang.Math.PI
import java.lang.Math.cos
import java.util.*
import kotlin.coroutines.experimental.buildIterator
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty

/**@author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */

fun debug(message: Any): Unit {
    Log.d("Out",message.toString())
    Coroutine.resumeOrCreate(::debug)
}

fun breath(seed: Int, min: Int, step: Int, max: Int): Iterator<Int> {
    return buildIterator<Int> {
        var sed: Double = seed * PI / (max - min)
        val step_rate = step * PI / (max - min)

        debug("$step_rate")
        while (true) {
            val dv = -(cos(sed) - 1)
            yield(((max - min) * dv / 2).toInt())
            sed += step_rate
        }
    }
}

object Coroutine{
    val state = HashMap<String, Array<KProperty<Any>>>()
    fun resumeOrCreate(func: KFunction<Any>): Unit {

    }
}