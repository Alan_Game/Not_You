package whu.alan.ny.util

/**@author Alan WANG
 * Created by Alan_WANG on 5/17/2017.
 */
fun Float.isInside(min:Float, max:Float):Boolean{
    return (this >= min)&&(this <=max)
}
fun Float.around(center:Float):Boolean{
    return (this>= center - 0.05f)&&(this <= center - 0.05f)
}