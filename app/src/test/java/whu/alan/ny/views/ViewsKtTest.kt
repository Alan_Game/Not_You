package whu.alan.ny.views

import org.junit.Assert.assertEquals
import org.junit.Test
import whu.alan.ny.util.breath

/**@author
 * Created by Alan_WANG on 5/16/2017.
 */
class ViewsKtTest {
    @Test
    fun breath() {
        val d = arrayOf(1,2,3,2,1,2,3,2,1)
        val idx = breath(1, 1, 1, 3)
        for (i in 0..8){
            println("test, $i")
            assertEquals(d[i],idx.next())
            println("ok, $i")
        }
    }

}